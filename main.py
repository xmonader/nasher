import os

import nasher
from flask import Flask

sites_path = "/opt/code/github/xmonader/nasher/sites"
examplesitepath = os.path.join(sites_path, "examplesite")
site = nasher.Site(examplesitepath, "examplesite")

sites = [site]

app = Flask(__name__, static_folder="/opt/code/github/threefoldtech/jumpscale_weblibs/static")
for site in sites:
    site.register_blueprint(app)

print(app.url_map)
app.run()