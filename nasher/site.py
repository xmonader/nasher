import os
import mistune 
from jinja2 import Environment, FileSystemLoader
from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound



def as_html(md):
    return mistune.markdown(md)


class Site:
    def __init__(self, path, name, mdcontent_dir=None, templates_dir=None):
        path = os.path.abspath(path)
        
        self.path = path
        self.name = name

        self.mdcontent_dir = mdcontent_dir or os.path.join(path, "md")
        self._pages_dir = os.path.join(self.mdcontent_dir, "pages")
        self._posts_dir = os.path.join(self.mdcontent_dir, "posts")
        self.static_dir = os.path.abspath(os.path.join(path, "static"))
        try:
            os.symlink("/opt/code/github/threefoldtech/jumpscale_weblibs/static/", self.static_dir, True)
        except Exception as ex:
            print("Err: ", ex)

        self.templates_dir = templates_dir or os.path.join(path, "templates") # reset with setter
        self.htmltemplatesenv = Environment(loader=FileSystemLoader(self.templates_dir))
        self.mdpagesenv = Environment(loader=FileSystemLoader(self._pages_dir))
        self.mdpostsenv = Environment(loader=FileSystemLoader(self._posts_dir))
        self.site_settings = {'companysname': name, 'email':'xmonader@dmdm.com'}

    def _render_file_with_jinja(self, env, filename, **ctx):
        template = env.get_template(filename)
        rendered = template.render(**ctx)
        return rendered
    
    def page_as_md(self, name, **ctx):
        rendered = self._render_file_with_jinja(self.mdpagesenv, name, **ctx)
        return rendered

    def post_as_md(self, name, **ctx):
        rendered = self._render_file_with_jinja(self.mdpostsenv, name, **ctx)
        return rendered
    
    def page_as_partial_html(self, name, **ctx):
        # 1- apply jinja on the md first.
        md = self.page_as_md(name, **ctx)
        html = as_html(md)
        return html

    def post_as_partial_html(self, name, **ctx):
        # 1- apply jinja on the md first.
        md = self.page_as_md(name, **ctx)
        html = as_html(md)
        return html 

    def page_as_html(self, name, htmlpage,  **ctx):
        html = self.page_as_partial_html(name, **ctx)
        ctx['htmlcontent'] = html

        rendered = self._render_file_with_jinja(self.htmltemplatesenv, htmlpage,**ctx)
        return rendered

    def post_as_html(self, name, htmlpage,  **ctx):
        html = self.post_as_partial_html(name, **ctx)
        ctx['htmlcontent'] = html
        rendered = self._render_file_with_jinja(self.htmltemplatesenv, htmlpage,**ctx)
        return rendered

    def register_blueprint(self, app):
        # FIXME: static folder for blueprints
        blueprint = Blueprint(self.name, __name__, template_folder=self.templates_dir, static_folder=self.static_dir)
        
        @blueprint.route("/{}/page/<pagename>".format(self.name))
        def get_page(pagename):
            if pagename.endswith('.md'):
                pagename = pagename[:-3]
            if pagename.endswith('.html'):
                pagename = pagename[:-5]
            return self.page_as_html(pagename+".md", pagename+".html", **self.site_settings)
        
        app.register_blueprint(blueprint)
    
    